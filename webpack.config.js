/**
 * Dependecies
 *
 * @author Edgar Figueroa
 * */

const path = require('path');

let config = {
    mode: 'development',
    entry:{
        app: path.resolve(__dirname, 'src/js/index.js')
    },
    output:{
        path: path.resolve(__dirname, 'dist/js'),
        filename: '[name].js'
    },
    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options:{
                        presets: ['env']
                    }
                }
            }
        ]
    }
}

module.exports = config;
